import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAcVN2Mm7C09UN5U-eUMnKa3mF5sK1BKck",
  authDomain: "yahyo-c30a5.firebaseapp.com",
  projectId: "yahyo-c30a5",
  storageBucket: "yahyo-c30a5.appspot.com",
  messagingSenderId: "769165282430",
  appId: "1:769165282430:web:42c4e384e96fd8e70d1c2f"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);


export const auth = getAuth();
export const db: any = getFirestore(app);
export const storage = getStorage(app);

